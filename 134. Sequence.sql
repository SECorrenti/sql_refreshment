

-- Sequence object
--       Introduced in SQL Server 2012
--       Generates sequence of numeric values in an ascending or descending order
-- Syntax :
-- CREATE SEQUENCE [schema_name . ] sequence_name
--     [ AS [ built_in_integer_type | user-defined_integer_type ] ]
--     [ START WITH <constant> ]
--     [ INCREMENT BY <constant> ]
--     [ { MINVALUE [ <constant> ] } | { NO MINVALUE } ]
--     [ { MAXVALUE [ <constant> ] } | { NO MAXVALUE } ]
--     [ CYCLE | { NO CYCLE } ]
--     [ { CACHE [ <constant> ] } | { NO CACHE } ]
--     [ ; ]




-- The following code create a Sequence object that starts with 1 and increments by 1

CREATE SEQUENCE [dbo].[SequenceObject] 
AS INT
START WITH 1
INCREMENT BY 1


-- ALTER SEQUENCE [dbo].[SequenceObject]
-- INCREMENT BY 10
-- MINVALUE 100
-- MAXVALUE 150
-- CYCLE





-- Now we have a sequence object created. To generate the sequence value use NEXT VALUE FOR clause

SELECT NEXT VALUE FOR [dbo].[SequenceObject]




-- Every time you execute the above query the sequence value will be incremented by 1. 
-- I executed the above query 5 times, so the current sequence value is 5.

-- Retrieving the current sequence value:
-- If you want to see what the current Sequence value before generating the next, use sys.sequences

SELECT * FROM sys.sequences WHERE name = 'SequenceObject'




-- Alter the Sequence object to reset the sequence value : 
ALTER SEQUENCE [SequenceObject] RESTART WITH 1






-- Using sequence value in an INSERT query : 

CREATE TABLE Employees
(
    Id INT PRIMARY KEY,
    Name NVARCHAR(50),
    Gender NVARCHAR(10)
)


-- Generate and insert Sequence values
INSERT INTO Employees VALUES
(NEXT VALUE for [dbo].[SequenceObject], 'Ben', 'Male')
INSERT INTO Employees VALUES
(NEXT VALUE for [dbo].[SequenceObject], 'Sara', 'Female')


-- Select the data from the table
SELECT * FROM Employees













































































































