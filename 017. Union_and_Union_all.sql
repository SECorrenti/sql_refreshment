


Create table tblIndiaCustomers (
	Id int primary key identity(1, 1),
	Name nvarchar(120) NOT NULL,
	Email nvarchar(150) NOT NULL
)


Create table tblUKCustomers (
	Id int primary key identity(1, 1),
	Name nvarchar(120) NOT NULL,
	Email nvarchar(150) NOT NULL
)

insert into tblIndiaCustomers values ('Raj', 'R@R.com')
insert into tblIndiaCustomers values ('Sam', 'S@S.com')

insert into tblUKCustomers values ('Ben', 'B@B.com')
insert into tblUKCustomers values ('Sam', 'S@S.com')



---- UNION Remove duplicate rows (Small issue: removing rows consuming time)
---- CTRL + L to Build an Execution plan
Select * from tblIndiaCustomers
Union
Select * from tblUKCustomers


---- UNION ALL retrieve all rows of both tables
Select * from tblIndiaCustomers
Union All
Select * from tblUKCustomers


--Note: For UNION and UNION ALL to work, the Number, Data types, 
--and the order of the columns in the select statements should be same




--	Difference between JOIN and UNION
--	JOINS and UNIONS are different things. However, this question is being asked 
--	very frequently now. UNION combines the result-set of two or more select 
--	queries into a single result-set which includes all the rows from all 
--	the queries in the union, where as JOINS, retrieve data from two or 
--	more tables based on logical relationships between the tables. 
--  In short, 
--	UNION combines rows from 2 or more tables, 
--	where 
--	JOINS combine columns from 2 or more table.






