

USE Sample3
GO

Select * from tblPerson



ALTER Table tblPerson 
Add Constraint UQ_tblPerson_Email
Unique(Email)


Insert into tblPerson values (2, 'XYZ', 'a@a.com', 1, 20)


Alter Table tblPerson
Drop Constraint UQ_tblPerson_Email


