

Create table tblEmployeeAudit (
Id int Primary Key Identity(1, 1),
AuditData nvarchar(500) NOT NULL
)



CREATE TRIGGER trEmployee_ForInsert
On tblEmployee
For Insert
As
Begin
	
	Declare @Id int
	Select @id = Id from inserted


	Insert into tblEmployeeAudit
	values (
	'New employee with Id = ' + CAST(@Id as nvarchar(5)) + ' is added at ' + CAST(GETDATE() as nvarchar(20))
	)

End



Insert into tblEmployee values ('Jimmy', 'Male', 5800, 'Israel', 1, 3, GETDATE())


select * from tblEmployeeAudit




--=============================================================================
--=============================================================================
--=============================================================================
--=============================================================================





CREATE TRIGGER trEmployee_ForDeleted
On tblEmployee
For Delete
As
Begin
	
	Declare @Id int
	Select @id = Id from deleted


	Insert into tblEmployeeAudit
	values (
	'New employee with Id = ' + CAST(@Id as nvarchar(5)) + ' is deleted at ' + CAST(GETDATE() as nvarchar(20))
	)

End



delete from tblEmployee where ID = 1
select * from tblEmployeeAudit





--=============================================================================
--=============================================================================
--=============================================================================
--=============================================================================



CREATE TRIGGER trEmployee_ForUpdate
On tblEmployee
For Update
As
Begin
	
	Declare @Id int
	Select @id = Id from deleted ----- old data row was updated
	Select @id = Id from inserted ----- new data row was updated


	Insert into tblEmployeeAudit
	values (
	'New employee with Id = ' + CAST(@Id as nvarchar(5)) + ' is deleted at ' + CAST(GETDATE() as nvarchar(20))
	)

End



--=============================================
--=============================================
--=============================================
--=============================================
--=============================================


Alter trigger tr_tblEmployee_ForUpdate
on tblEmployee
for Update
as
Begin
      -- Declare variables to hold old and updated data
      Declare @Id int
      Declare @OldName nvarchar(20), @NewName nvarchar(20)
      Declare @OldSalary int, @NewSalary int
      Declare @OldGender nvarchar(20), @NewGender nvarchar(20)
      Declare @OldDeptId int, @NewDeptId int
     
      -- Variable to build the audit string
      Declare @AuditString nvarchar(1000)
      
      -- Load the updated records into temporary table
      Select *
      into #TempTable
      from inserted
     
      -- Loop thru the records in temp table
      While(Exists(Select Id from #TempTable))
      Begin
            --Initialize the audit string to empty string
            Set @AuditString = ''
           
            -- Select first row data from temp table
            Select Top 1 @Id = Id, @NewName = Name, 
            @NewGender = Gender, @NewSalary = Salary,
            @NewDeptId = DepartmentId
            from #TempTable
           
            -- Select the corresponding row from deleted table
            Select @OldName = Name, @OldGender = Gender, 
            @OldSalary = Salary, @OldDeptId = DepartmentId
            from deleted where Id = @Id
   
     -- Build the audit string dynamically           
            Set @AuditString = 'Employee with Id = ' + Cast(@Id as nvarchar(4)) + ' changed'
            if(@OldName <> @NewName)
                  Set @AuditString = @AuditString + ' NAME from ' + @OldName + ' to ' + @NewName
                 
            if(@OldGender <> @NewGender)
                  Set @AuditString = @AuditString + ' GENDER from ' + @OldGender + ' to ' + @NewGender
                 
            if(@OldSalary <> @NewSalary)
                  Set @AuditString = @AuditString + ' SALARY from ' + Cast(@OldSalary as nvarchar(10))+ ' to ' + Cast(@NewSalary as nvarchar(10))
                  
     if(@OldDeptId <> @NewDeptId)
                  Set @AuditString = @AuditString + ' DepartmentId from ' + Cast(@OldDeptId as nvarchar(10))+ ' to ' + Cast(@NewDeptId as nvarchar(10))
           
            insert into tblEmployeeAudit values(@AuditString)
            
            -- Delete the row from temp table, so we can move to the next row
            Delete from #TempTable where Id = @Id
      End
End




-- Instead of insert trigger  ---- INSERT

Create trigger tr_vWEmployeeDetails_InsteadOfInsert
on vWEmployeeDetails
Instead Of Insert
as
Begin
      Declare @DeptId int
      
      --Check if there is a valid DepartmentId
      --for the given DepartmentName
      Select @DeptId = DeptId 
      from tblDepartment 
      join inserted
      on inserted.DeptName = tblDepartment.DeptName
      
      --If DepartmentId is null throw an error
      --and stop processing
      if(@DeptId is null)
      Begin
      Raiserror('Invalid Department Name. Statement terminated', 16, 1)
      return
      End
      
      --Finally insert into tblEmployee table
      Insert into tblEmployee(Id, Name, Gender, DepartmentId)
      Select Id, Name, Gender, @DeptId
      from inserted
End


Insert into vWEmployeeDetails values(7, 'Valarie', 'Female', 'IT')








--=============================================
--=============================================
--=============================================
--=============================================
--=============================================




-- Instead of insert trigger  ---- UPDATE

Create Trigger tr_vWEmployeeDetails_InsteadOfUpdate
on vWEmployeeDetails
instead of update
as
Begin
	 -- if EmployeeId is updated
	 if(Update(Id))
	 Begin
		  Raiserror('Id cannot be changed', 16, 1)
		  Return
	 End
 
	 -- If DeptName is updated
	 if(Update(DeptName)) 
	 Begin
		  Declare @DeptId int

		  Select @DeptId = DeptId
		  from tblDepartment
		  join inserted
		  on inserted.DeptName = tblDepartment.DeptName
  
		  if(@DeptId is NULL )
		  Begin
			   Raiserror('Invalid Department Name', 16, 1)
			   Return
		  End
  
		  Update tblEmployee set DepartmentId = @DeptId
		  from inserted
		  join tblEmployee
		  on tblEmployee.Id = inserted.id
	 End
 
	 -- If gender is updated
	 if(Update(Gender))
	 Begin
		  Update tblEmployee set Gender = inserted.Gender
		  from inserted
		  join tblEmployee
		  on tblEmployee.Id = inserted.id
	 End
 
	 -- If Name is updated
	 if(Update(Name))
	 Begin
		  Update tblEmployee set Name = inserted.Name
		  from inserted
		  join tblEmployee
		  on tblEmployee.Id = inserted.id
	 End
End



-- Instead of insert trigger  ---- DELETE


Create Trigger tr_vWEmployeeDetails_InsteadOfDelete
on vWEmployeeDetails
instead of delete
as
Begin
 Delete tblEmployee 
 from tblEmployee
 join deleted
 on tblEmployee.Id = deleted.Id
 
 --Subquery
 --Delete from tblEmployee 
 --where Id in (Select Id from deleted)
End



