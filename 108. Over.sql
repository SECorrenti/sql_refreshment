


-- The OVER clause combined with PARTITION BY is used to break up data into partitions. 
-- Syntax : function (...) OVER (PARTITION BY col1, Col2, ...)


-- One way to achieve this is by including the aggregations in a subquery and then JOINING it 
-- with the main query as shown in the example below. 
-- Look at the amount of T-SQL code we have to write.
SELECT  Name, Salary, 
        Employees.Gender, Genders.GenderTotals,
        Genders.AvgSal, Genders.MinSal, 
        Genders.MaxSal   
FROM Employees
INNER JOIN
    (
      SELECT Gender,  COUNT(*) AS GenderTotals,
                      AVG(Salary) AS AvgSal,
                      MIN(Salary) AS MinSal, 
                      MAX(Salary) AS MaxSal
      FROM Employees
      GROUP BY Gender
    ) AS Genders
ON Genders.Gender = Employees.Gender




-- Better way of doing this is by using the OVER clause combined with PARTITION BY
SELECT Name, Salary, Gender,
        COUNT(Gender) OVER(PARTITION BY Gender) AS GenderTotals,
        AVG(Salary) OVER(PARTITION BY Gender) AS AvgSal,
        MIN(Salary) OVER(PARTITION BY Gender) AS MinSal,
        MAX(Salary) OVER(PARTITION BY Gender) AS MaxSal
FROM Employees