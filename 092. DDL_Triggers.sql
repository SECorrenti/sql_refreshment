


-- In SQL Server there are 4 types of triggers
-- 1. DML Triggers - Data Manipulation Language. Discussed in Parts 43 to 47 of SQL Server Tutorial.
-- 2. DDL Triggers - Data Definition Language
-- 3. CLR triggers - Common Language Runtime
-- 4. Logon triggers



-- What are DDL triggers
-- DDL triggers fire in response to DDL events - CREATE, ALTER, and DROP 
-- (Table, Function, Index, Stored Procedure etc...). For the list of all DDL events 
-- please visit https://msdn.microsoft.com/en-us/library/bb522542.aspx

-- Certain system stored procedures that perform DDL-like operations 
-- can also fire DDL triggers. Example - sp_rename system stored procedure

-- What is the use of DDL triggers
-- If you want to execute some code in response to a specific DDL event
-- To prevent certain changes to your database schema
-- Audit the changes that the users are making to the database structure



-- Syntax for creating DDL trigger
-- CREATE TRIGGER [Trigger_Name]
-- ON [Scope (Server|Database)]
-- FOR [EventType1, EventType2, EventType3, ...],
-- AS
-- BEGIN
--    -- Trigger Body
-- END



-- DDL triggers scope : DDL triggers can be created in a specific database or at the server level. 




CREATE TRIGGER trMyFirstTrigger
ON Database
FOR CREATE_TABLE, ALTER_TABLE, DROP_TABLE
AS
BEGIN
   Rollback 
   Print 'You cannot create, alter or drop a table'
END




-- To disable trigger
-- 1. Right click on the trigger in object explorer and select "Disable" from the context menu 
-- 2. You can also disable the trigger using the following T-SQL command
DISABLE TRIGGER trMyFirstTrigger ON DATABASE

-- To enable trigger
-- 1. Right click on the trigger in object explorer and select "Enable" from the context menu 
-- 2. You can also enable the trigger using the following T-SQL command
ENABLE TRIGGER trMyFirstTrigger ON DATABASE

-- To drop trigger
-- 1. Right click on the trigger in object explorer and select "Delete" from the context menu 
-- 2. You can also drop the trigger using the following T-SQL command
DROP TRIGGER trMyFirstTrigger ON DATABASE

-- Certain system stored procedures that perform DDL-like operations can also fire DDL triggers. 
-- The following trigger will be fired when ever you rename a database object using sp_rename system stored procedure.

CREATE TRIGGER trRenameTable
ON DATABASE
FOR RENAME
AS
BEGIN
    Print 'You just renamed something'
END





CREATE TRIGGER tr_ServerScopeTrigger
ON ALL SERVER --            <------ BLOCK TO ENTIRE SQL DATABASES
FOR CREATE_TABLE, ALTER_TABLE, DROP_TABLE
AS
BEGIN
    ROLLBACK
    Print 'You cannot create, alter or drop a table in any database on the server'
END

-- Where can I find the Server-scoped DDL triggers
-- 1. In the Object Explorer window, expand "Server Objects" folder
-- 2. Expand Triggers folder



--==========================================================
--==========================================================
--==========================================================
--==========================================================
--==========================================================
--==========================================================
--==========================================================



-- order executions - part 94

EXEC sp_settriggerorder
@triggername = 'tr_DatabaseScopeTrigger1',
@order = 'none',
@stmttype = 'CREATE_TABLE',
@namespace = 'DATABASE'
GO






-- Table to store the audit data
Create table TableChanges
(
    DatabaseName nvarchar(250),
    TableName nvarchar(250),
    EventType nvarchar(250),
    LoginName nvarchar(250),
    SQLCommand nvarchar(2500),
    AuditDateTime datetime
)
Go

-- The following trigger audits all table changes in all databases on a SQL Server
CREATE TRIGGER tr_AuditTableChanges
ON ALL SERVER
FOR CREATE_TABLE, ALTER_TABLE, DROP_TABLE
AS
BEGIN
    DECLARE @EventData XML
    SELECT @EventData = EVENTDATA()

    INSERT INTO SampleDB.dbo.TableChanges
    (DatabaseName, TableName, EventType, LoginName,
     SQLCommand, AuditDateTime)
    VALUES
    (
         @EventData.value('(/EVENT_INSTANCE/DatabaseName)[1]', 'varchar(250)'),
         @EventData.value('(/EVENT_INSTANCE/ObjectName)[1]', 'varchar(250)'),
         @EventData.value('(/EVENT_INSTANCE/EventType)[1]', 'nvarchar(250)'),
         @EventData.value('(/EVENT_INSTANCE/LoginName)[1]', 'varchar(250)'),
         @EventData.value('(/EVENT_INSTANCE/TSQLCommand)[1]', 'nvarchar(2500)'),
         GetDate()
    )
END

-- In the above example we are using EventData() function which returns 
-- event data in XML format. The following XML is returned by the 
-- EventData() function when I created a table 
-- with name = MyTable in SampleDB database.

-- <EVENT_INSTANCE>
--   <EventType>CREATE_TABLE</EventType>
--   <PostTime>2015-09-11T16:12:49.417</PostTime>
--   <SPID>58</SPID>
--   <ServerName>VENKAT-PC</ServerName>
--   <LoginName>VENKAT-PC\Tan</LoginName>
--   <UserName>dbo</UserName>
--   <DatabaseName>SampleDB</DatabaseName>
--   <SchemaName>dbo</SchemaName>
--   <ObjectName>MyTable</ObjectName>
--   <ObjectType>TABLE</ObjectType>
--   <TSQLCommand>
--     <SetOptions ANSI_NULLS="ON" ANSI_NULL_DEFAULT="ON"
--                 ANSI_PADDING="ON" QUOTED_IDENTIFIER="ON"
--                 ENCRYPTED="FALSE" />
--     <CommandText>
--       Create Table MyTable
--       (
--          Id int,
--          Name nvarchar(50),
--          Gender nvarchar(50)
--       )
--     </CommandText>
--   </TSQLCommand>
-- </EVENT_INSTANCE>




















