





Select * from tblEmployee



CREATE Index IX_tblEmployee_Salary
ON tblEmployee (Salary ASC)




DROP Index tblEmployee.IX_tblEmployee_Salary



sp_helpIndex tblEmployee





--	1. Clustered Index:
--			A clustered index determines the physical order of data in a table. 
--			For this reason, a table can have only one clustered index. 
--			Primary Key for example is a clustered Index, this is the 
--			reason all the columns are ordered when we insert a row


-- You can create an Clustered index by multiple columns
Create Clustered Index IX_tblEmployee_FirstByGender_ThenBySalary
On tblEmployee(Gender DESC, Salary ASC)




-- 2. NON CLUSTERED INDEX
--			Save the data in different table
Create NONClustered Index IX_tblEmployee_FirstByGender_ThenBySalary
On tblEmployee(Gender DESC, Salary ASC)




-- 3. Unique -- force the column to be unique
Alter Table tblEmployee
Add Constraint UQ_tblEmployee_City
UNIQUE CLUSTERED(City)














