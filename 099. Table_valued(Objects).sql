



-- table valued parameters in SQL Server.

-- Table Valued Parameter is a new feature introduced in SQL SERVER 2008.
-- Table Valued Parameter allows a table (i.e multiple rows of data)
-- to be passed as a parameter to a stored procedure from T-SQL code
-- or from an application. 
-- Prior to SQL SERVER 2008, it is not possible to pass a 
-- table variable as a parameter to a stored procedure.

-- Let us understand how to pass multiple rows to a stored procedure using Table Valued Parameter
-- with an example. We want to insert multiple rows into the following Employees table.
-- At the moment this table does not have any rows.



-- SQL Script to create the Employees table
Create Table Employees
(
    Id int primary key,
    Name nvarchar(50),
    Gender nvarchar(10)
)
GO
-- Step 1: Create User-defined Table Type

CREATE TYPE EmpTableType AS TABLE
(
    Id INT PRIMARY KEY,
    Name NVARCHAR(50),
    Gender NVARCHAR(10)
)
GO
-- Step 2: Use the User-defined Table Type as a parameter in the stored procedure.
-- Table valued parameters must be passed as read-only to stored procedures,
-- functions etc. This means you cannot perform DML operations like INSERT, UPDATE or DELETE
-- on a table-valued parameter in the body of a function, stored procedure etc.

CREATE PROCEDURE spInsertEmployees
@EmpTableType EmpTableType READONLY
AS
BEGIN
    INSERT INTO Employees
    SELECT * FROM @EmpTableType
END



-- Step 3: Declare a table variable, insert the data and then pass the table variable as a parameter to the stored procedure.

DECLARE @EmployeeTableType EmpTableType

INSERT INTO @EmployeeTableType VALUES (1, 'Mark', 'Male')
INSERT INTO @EmployeeTableType VALUES (2, 'Mary', 'Female')
INSERT INTO @EmployeeTableType VALUES (3, 'John', 'Male')
INSERT INTO @EmployeeTableType VALUES (4, 'Sara', 'Female')
INSERT INTO @EmployeeTableType VALUES (5, 'Rob', 'Male')

EXECUTE spInsertEmployees @EmployeeTableType






-- protected void btnInsert_Click(object sender, EventArgs e)
-- {
--     string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
--     using (SqlConnection con = new SqlConnection(cs))
--     {
--         SqlCommand cmd = new SqlCommand("spInsertEmployees", con);
--         cmd.CommandType = CommandType.StoredProcedure;

--         SqlParameter paramTVP = new SqlParameter()
--         {
--             ParameterName = "@EmpTableType",
--             Value = GetEmployeeData()
--         };
--         cmd.Parameters.Add(paramTVP);

--         con.Open();
--         cmd.ExecuteNonQuery();
--         con.Close();
--     }
-- }




-- private DataTable GetEmployeeData()
-- {
--     DataTable dt = new DataTable();
--     dt.Columns.Add("Id");
--     dt.Columns.Add("Name");
--     dt.Columns.Add("Gender");

--     dt.Rows.Add(txtId1.Text, txtName1.Text, txtGender1.Text);
--     dt.Rows.Add(txtId2.Text, txtName2.Text, txtGender2.Text);
--     dt.Rows.Add(txtId3.Text, txtName3.Text, txtGender3.Text);
--     dt.Rows.Add(txtId4.Text, txtName4.Text, txtGender4.Text);
--     dt.Rows.Add(txtId5.Text, txtName5.Text, txtGender5.Text);

--     return dt;
-- }













