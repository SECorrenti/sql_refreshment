

-- MERGE statement in SQL Server
--    Merge statement introduced in SQL Server 2008 allows us 
--    to perform Inserts, Updates and Deletes in one statement. 
--    This means we no longer have to use multiple statements 
--    for performing Insert, Update and Delete



-- Merge statement syntax
-- MERGE [TARGET] AS T
-- USING [SOURCE] AS S
--    ON [JOIN_CONDITIONS]
--  WHEN MATCHED THEN 
--       [UPDATE STATEMENT]
--  WHEN NOT MATCHED BY TARGET THEN
--       [INSERT STATEMENT] 
--  WHEN NOT MATCHED BY SOURCE THEN
--       [DELETE STATEMENT]



Create table StudentSource
(
     ID int primary key,
     Name nvarchar(20)
)
GO

Insert into StudentSource values (1, 'Mike')
Insert into StudentSource values (2, 'Sara')
GO

Create table StudentTarget
(
     ID int primary key,
     Name nvarchar(20)
)
GO

Insert into StudentTarget values (1, 'Mike M')
Insert into StudentTarget values (3, 'John')
GO

MERGE StudentTarget AS T
USING StudentSource AS S
ON T.ID = S.ID
WHEN MATCHED THEN
     UPDATE SET T.NAME = S.NAME
WHEN NOT MATCHED BY TARGET THEN
     INSERT (ID, NAME) VALUES(S.ID, S.NAME)
-- WHEN NOT MATCHED BY SOURCE THEN --   <-------------- DO NOT USE THIS STATEMENT
--      DELETE;

-- (In real time we mostly perform INSERTS and UPDATES. 
-- The rows that are present in target table but not in source
--  table are usually not deleted from the target table.)











