



Create table tblEmployee(
	ID int Primary key Identity(1,1),
	Name nvarchar(120) NOT NULL,
	Gender nvarchar(50) NOT NULL,
	Salary int Not Null,
	City nvarchar(120) Not Null
)

--insert into tblEmployee values ('Tom', 'Male', 4000, 'London');
--insert into tblEmployee values ('Pam', 'Female', 3000, 'New York');
--insert into tblEmployee values ('John', 'Male', 3500, 'London');
--insert into tblEmployee values ('Sam', 'Male', 4500, 'London');
--insert into tblEmployee values ('Todd', 'Male', 2800, 'Sydney');
--insert into tblEmployee values ('Ben', 'Male', 7000, 'ney York');
--insert into tblEmployee values ('Sara', 'Female', 4800, 'London');
--insert into tblEmployee values ('Valarie', 'Female', 5500, 'London');
--insert into tblEmployee values ('James', 'Male', 6500, 'London');
--insert into tblEmployee values ('Russell', 'Male', 8800, 'London');



Select * from tblEmployee



Select City, Sum(Salary) as TotalSalary
from tblEmployee
Group by City



Select City, Gender, Sum(Salary) as TotalSalary
from tblEmployee
Group by City, Gender
Order by City



Select City, Gender, Sum(Salary) as TotalSalary, COUNT(ID) as [Total Employees]
from tblEmployee
Group by City, Gender
Order by City



Select City, Gender, Sum(Salary) as TotalSalary, COUNT(ID) as [Total Employees]
from tblEmployee
Where Gender = 'Male'
Group by City, Gender
Order by City



Select City, Gender, Sum(Salary) as TotalSalary, COUNT(ID) as [Total Employees]
from tblEmployee
Group by City, Gender
Having Gender = 'Male'

--Having vs Where
--===============
--From a performance standpoint, you cannot say that one method is less efficient 
--than the other. Sql server optimizer analyzes each statement and selects 
--an efficient way of executing it. As a best practice, use the syntax 
--that clearly describes the desired result. Try to eliminate rows that 
--you wouldn't need, as early as possible.


--Difference between WHERE and HAVING clause:
--===========================================
--1.	WHERE clause can be used with - Select, Insert, and Update statements,
--	where as HAVING clause can only be used with the Select statement.
--2.	WHERE filters rows before aggregation (GROUPING), where as, HAVING 
--	filters groups, after the aggregations are performed.
--3.	Aggregate functions cannot be used in the WHERE clause, unless it is in 
--	a sub query contained in a HAVING clause, whereas, aggregate functions 
--	can be used in Having clause.


