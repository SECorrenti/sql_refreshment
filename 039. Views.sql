



-- View is actually a virtual table
--		You can take the data of any table and hide some information





Create View vw_EmployeesByDepartments
as
Select E.ID, Name, Salary, Gender, DeparmentName
From tblEmployee E
join tblDepartment D
on E.DepatmentId = D.ID




Select * from vw_EmployeesByDepartments



-- Update from a View

Update vw_EmployeesByDepartments
Set Name = 'Beny' where ID = 6



-- Delete from a View

Delete from vw_EmployeesByDepartments where Id = 6



-- Insert To a View

--Insert into vw_EmployeesByDepartments values(....)





--Create view vWTotalSalesByProduct
--with SchemaBinding
--as
--Select Name, 
--SUM(ISNULL((QuantitySold * UnitPrice), 0)) as TotalSales, 
--COUNT_BIG(*) as TotalTransactions
--from dbo.tblProductSales
--join dbo.tblProduct
--on dbo.tblProduct.ProductId = dbo.tblProductSales.ProductId
--group by Name



--Create Unique Clustered Index UIX_vWTotalSalesByProduct_Name
--on vWTotalSalesByProduct(Name)







-- Limitations of views

-- 1. You cannot pass parameters to a view. Table Valued functions are an excellent replacement for parameterized views.
-- 2. Rules and Defaults cannot be associated with views.
-- 3. The ORDER BY clause is invalid in views unless TOP or FOR XML is also specified.
-- 4. Views cannot be based on temporary tables.




