








-- Select CTE - (Common table expression)


With Employee_Name_Gender
as 
(
  Select ID, Name, Gender from tblEmployee
)
SELECT * from Employee_Name_Gender







-- Update CTE - (Common table expression)


With Employee_Name_Gender
as 
(
  select Id, Name, Gender from tblEmployee
)


--1. A CTE is based on a single base table, then the UPDATE suceeds and works as expected.
--2. A CTE is based on more than one base table, and if the UPDATE affects multiple base tables,
--   the update is not allowed and the statement terminates with an error.
--3. A CTE is based on more than one base table, and if the UPDATE affects only one base table,
--   the UPDATE succeeds(but not as expected always)







-- Recursive CTE - (Common table expression)

With
  EmployeesCTE (EmployeeId, Name, ManagerId, [Level])
  as
  (
    Select EmployeeId, Name, ManagerId, 1
    from tblEmployee
    where ManagerId is null
    
    union all
    
    Select tblEmployee.EmployeeId, tblEmployee.Name, 
    tblEmployee.ManagerId, EmployeesCTE.[Level] + 1
    from tblEmployee
    join EmployeesCTE --					<============= RECURSIVE CTE
    on tblEmployee.ManagerID = EmployeesCTE.EmployeeId
  )
Select EmpCTE.Name as Employee, Isnull(MgrCTE.Name, 'Super Boss') as Manager, 
EmpCTE.[Level] 
from EmployeesCTE EmpCTE
left join EmployeesCTE MgrCTE
on EmpCTE.ManagerId = MgrCTE.EmployeeId
