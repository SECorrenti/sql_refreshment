






class Sample {


// To retrieve the second result-set from SqlDataReader object, use the NextResult() 
// as shown in the code snippet below. The NextResult() method returns true and advances to the next result-set. 

private void Method() 
{

    string ConnectionString = ConfigurationManager.ConnectionStrings["DBConnectionString"].ConnectionString;
    using (SqlConnection connection = new SqlConnection(ConnectionString))
    {
        connection.Open();
        SqlCommand command = new SqlCommand("select * from tblProductInventory; select * from tblProductCategories", connection);
        using (SqlDataReader reader = command.ExecuteReader())
        {
            ProductsGridView.DataSource = reader;
            ProductsGridView.DataBind();

            while (reader.NextResult()) // <------ NEXT OBJECT 
            {
                CategoriesGridView.DataSource = reader;
                CategoriesGridView.DataBind();
            }
       }
    }

}


