




Select ISNULL(NULL, 'Nothing here') 
Select COALESCE(NULL, 'Nothing here') 


Select E.Name as EmployeeName, ISNULL(M.Name, 'No Manager') as ManagerName
from tblEmployee E
Left Join tblEmployee M
on E.ManagerID = M.ID



Select E.Name as EmployeeName, 
	CASE 
		WHEN M.Name IS NULL 
		THEN 'No Manager' 
		ELSE M.Name END 
	as ManagerName
from tblEmployee E
Left Join tblEmployee M
on E.ManagerID = M.ID



-- COALESCE() Return the first NOT NULL value.
Select Id, COALESCE(Name, Gender, City) from tblEmployee
