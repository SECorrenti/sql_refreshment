


-- Create a Local Temporary Table:
--		Creating a local Temporary table is very similar 
--		to creating a permanent table, except that you prefix 
--		the table name with 1 pound (#) symbol. 
--		In the example below, #PersonDetails is a 
--		local temporary table, with Id and Name columns.



CREATE Table #PersonDetails(Id int, Name nvarchar(20))



Insert into #PersonDetails Values (1, 'Mike')
Insert into #PersonDetails Values (2, 'John')
Insert into #PersonDetails Values (3, 'Todd')



Select * from #PersonDetails




-- A way to check if the temporary table is created.

Select name from tempdb..sysobjects where name like '#PersonDetails%'




-- The Temporary table is deleted when the conection of the query done

-- Only the connection query how are create the table can access to it.

  


  
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================





-- Use Double ## to create a Global Table for all Stable Connections
  


CREATE Table ##GlobalPersonDetails(Id int, Name nvarchar(20))



Insert into ##GlobalPersonDetails Values (1, 'Mike')
Insert into ##GlobalPersonDetails Values (2, 'John')
Insert into ##GlobalPersonDetails Values (3, 'Todd')



Select * from ##GlobalPersonDetails





  
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================
--===============================================================================




-- Derived tables and common table expressions in sql server (Part 48)

-- using temporary tables

Select DeptName, DepartmentId, COUNT(*) as TotalEmployees
into #TempEmployeeCount
from tblEmployee
join tblDepartment
on tblEmployee.DepartmentId = tblDepartment.DeptId
group by DeptName, DepartmentId

Select DeptName, TotalEmployees
From #TempEmployeeCount
where TotalEmployees >= 2

Drop Table #TempEmployeeCount





-- Using Table Variable:

Declare @tblEmployeeCount table
(DeptName nvarchar(20),DepartmentId int, TotalEmployees int)

Insert @tblEmployeeCount
Select DeptName, DepartmentId, COUNT(*) as TotalEmployees
from tblEmployee
join tblDepartment
on tblEmployee.DepartmentId = tblDepartment.DeptId
group by DeptName, DepartmentId

Select DeptName, TotalEmployees
From @tblEmployeeCount
where  TotalEmployees >= 2


-- Note:  Just like TempTables, a table variable is also created in TempDB.
--        The scope of a table variable is the batch, stored procedure,
--        or statement block in which it is declared. 
--        ADV: They can be passed as parameters between procedures 
--             & you not need to drop it to TempDB Database.





-- Using Derived Tables

Select DeptName, TotalEmployees
from 
 (
  Select DeptName, DepartmentId, COUNT(*) as TotalEmployees
  from tblEmployee
  join tblDepartment
  on tblEmployee.DepartmentId = tblDepartment.DeptId
  group by DeptName, DepartmentId
 ) 
as EmployeeCount
where TotalEmployees >= 2






-- Using CTE - (Common table expression)

With EmployeeCount(DepartmentId, TotalEmployees)
as
(
 Select DepartmentId, COUNT(*) as TotalEmployees
 from tblEmployee
 group by DepartmentId
)

Select DeptName, TotalEmployees
from tblDepartment
join EmployeeCount
on tblDepartment.DeptId = EmployeeCount.DepartmentId
order by TotalEmployees


-- Note: Common table expression (CTE) is introduced in SQL server 2005. 
--       A CTE is a temporary result set, that can be referenced within 
--       a SELECT, INSERT, UPDATE, or DELETE statement, 
--       that immediately follows the CTE.
-- Use CTE as Inmediate query select, otherwise the table will throw an error
--       Error: Common table expression defined but not used.







-- It is also, possible to create multiple CTE's using a single WITH clause.

With EmployeesCountBy_Payroll_IT_Dept(DepartmentName, Total)
as
(
 Select DeptName, COUNT(Id) as TotalEmployees
 from tblEmployee
 join tblDepartment 
 on tblEmployee.DepartmentId = tblDepartment.DeptId
 where DeptName IN ('Payroll','IT')
 group by DeptName
),
EmployeesCountBy_HR_Admin_Dept(DepartmentName, Total)
as
(
 Select DeptName, COUNT(Id) as TotalEmployees
 from tblEmployee
 join tblDepartment 
 on tblEmployee.DepartmentId = tblDepartment.DeptId
 group by DeptName 
)
Select * from EmployeesCountBy_HR_Admin_Dept 
UNION
Select * from EmployeesCountBy_Payroll_IT_Dept



