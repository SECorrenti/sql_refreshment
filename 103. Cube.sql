
-- Cube() in SQL Server produces the result set by generating 
-- all combinations of columns specified in GROUP BY CUBE(). 



SELECT Country, Gender, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY Cube(Country, Gender)



SELECT Country, Gender, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY Country, Gender With Cube






-- The above query is equivalent to the following Grouping Sets query
SELECT Country, Gender, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY
    GROUPING SETS
    (
         (Country, Gender),
         (Country),
         (Gender),
         ()
    )








-- The above query is equivalent to the following UNION ALL query. 
-- While the data in the result set is the same, the ordering is not. 
-- Use ORDER BY to control the ordering of rows in the result set.


SELECT Country, Gender, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY Country, Gender

UNION ALL

SELECT Country, NULL, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY Country

UNION ALL

SELECT NULL, Gender, SUM(Salary) AS TotalSalary
FROM Employees
GROUP BY Gender

UNION ALL

SELECT NULL, NULL, SUM(Salary) AS TotalSalary
FROM Employees





-- Difference between cube and rollup

-- CUBE generates a result set that shows aggregates for all combinations
-- of values in the selected columns, where as ROLLUP generates a result
-- set that shows aggregates for a hierarchy of values in the selected columns.


SELECT Continent, Country, City, SUM(SaleAmount) AS TotalSales
FROM Sales
GROUP BY ROLLUP(Continent, Country, City)

-- ROLLUP(Continent, Country, City) produces Sum of Salary for the following hierarchy
-- Continent, Country, City
-- Continent, Country, 
-- Continent
-- ()


SELECT Continent, Country, City, SUM(SaleAmount) AS TotalSales
FROM Sales
GROUP BY CUBE(Continent, Country, City)

-- CUBE(Continent, Country, City) produces Sum of Salary for all the following column combinations
-- Continent, Country, City
-- Continent, Country, 
-- Continent, City
-- Continent
-- Country, City
-- Country,
-- City
-- ()








-- Grouping ROLLBACK

SELECT   Continent, Country, City, SUM(SaleAmount) AS TotalSales,
         GROUPING(Continent) AS GP_Continent,
         GROUPING(Country) AS GP_Country,
         GROUPING(City) AS GP_City
FROM Sales
GROUP BY ROLLUP(Continent, Country, City)





-- Replace NULLs

SELECT  
    CASE WHEN
         GROUPING(Continent) = 1 THEN 'All' ELSE ISNULL(Continent, 'Unknown')
    END AS Continent,
    CASE WHEN
         GROUPING(Country) = 1 THEN 'All' ELSE ISNULL(Country, 'Unknown')
    END AS Country,
    CASE
         WHEN GROUPING(City) = 1 THEN 'All' ELSE ISNULL(City, 'Unknown')
    END AS City,
    SUM(SaleAmount) AS TotalSales
FROM Sales
GROUP BY ROLLUP(Continent, Country, City)






-- Calculating NULLs  
-- GROUPING_ID function computes the level of grouping.

SELECT   Continent, Country, City, SUM(SaleAmount) AS TotalSales,
         CAST(GROUPING(Continent) AS NVARCHAR(1)) +
         CAST(GROUPING(Country) AS NVARCHAR(1)) +
         CAST(GROUPING(City) AS NVARCHAR(1)) AS Groupings,
         GROUPING_ID(Continent, Country, City) AS GPID
FROM Sales
GROUP BY ROLLUP(Continent, Country, City)

--=========

SELECT   Continent, Country, City, SUM(SaleAmount) AS TotalSales,
         GROUPING_ID(Continent, Country, City) AS GPID
FROM Sales
GROUP BY ROLLUP(Continent, Country, City)
ORDER BY GPID










 

