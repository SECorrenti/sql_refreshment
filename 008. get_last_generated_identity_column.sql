create table test1 (
	[ID] int primary key identity(1, 1),
	[Value] nvarchar(150) Not null
)

create table test2 (
	[ID] int primary key identity(1, 1),
	[Value] nvarchar(150) Not null
)


Insert into test1 Values('x')

Insert into test2 Values('z')


CREATE TRIGGER TR_ForInsetIntoTest1 ON test1 FOR INSERT
AS
BEGIN
	Insert into test1 Values('y')
END


-- Retrieve the last insert identity in the same session and the same scope
SELECT SCOPE_IDENTITY()

-- Retrieve the last insert identity in the same session and the ACROSS ANY SCOPE 
SELECT @@IDENTITY

-- Retrieve the last insert identity ACROSS ANY SESSION and the ACROSS ANY SCOPE 
SELECT IDENT_CURRENT('test2')


select * from test1
select * from test2



