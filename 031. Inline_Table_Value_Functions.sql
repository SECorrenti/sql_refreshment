





--	Inline Table Valued function(ILTVF):

CREATE FUNCTION fn_EmployeeByGender(@Gender nvarchar(10))
RETURNS TABLE
AS
RETURN (
	Select Id, Name, Cast(DateOfBirth as Date) as DOB
	from tblEmployee where Gender = @Gender
)


Select * from dbo.fn_EmployeeByGender('female')






--	Multi-statement Table Valued function(MSTVF):

CREATE FUNCTION fn_MSTVF_GetEmployees() 
Returns @Table Table (Id int, Name nvarchar(20), DOB Date)
as
Begin
	Insert into @Table
	Select Id, Name, Cast(DateOfBirth as Date) from tblEmployee 

	RETURN
END


Select * from fn_MSTVF_GetEmployees()



