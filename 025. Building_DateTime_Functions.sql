





Select GETDATE() as GETDATE -- Commonly used function


Select CURRENT_TIMESTAMP as [CURRENT_TIMESTAMP] -- ANSI SQL equivalent to GETDATE


Select SYSDATETIME() as SYSDATETIME -- More fractional seconds precision


Select SYSDATETIMEOFFSET() as SYSDATETIMEOFFSET -- More fractional seconds precision + Time zone offset


Select GETUTCDATE() as GETUTCDATE	--	UTC Date and Time


Select SYSUTCDATETIME() as SYSUTCDATETIME -- UTC Date and Time, with More fractional seconds precision


Select ISDATE(GETDATE())


Select DAY(GETDATE())


Select MONTH(GETDATE())


Select YEAR(GETDATE())


Select DATENAME(DAY, GETDATE())


Select DATENAME(WEEKDAY, GETDATE())


Select DATENAME(MONTH, GETDATE())


Select DATENAME(YEAR, GETDATE())
Select DATENAME(yy, GETDATE())
Select DATENAME(yyyy, GETDATE())


Select DATEPART(MONTH, GETDATE())
Select MONTH(GETDATE())


Select DATEADD(DAY, 5, GETDATE())

Select DATEDIFF(DAY, GETDATE(), DATEADD(DAY, 5, GETDATE()));





































