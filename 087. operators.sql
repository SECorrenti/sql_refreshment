

-- EXCEPT operator --- Get The NON Equals columns between two tables

SELECT Id, Name, Gender from TableB

EXCEPT

SELECT Id, Name, Gender from TableA

--==============================================

SELECT Id, Name, Gender, Salary from tblEmployees where Salary >= 50000
EXCEPT
SELECT Id, Name, Gender, Salary from tblEmployees where Salary >= 60000
ORDER BY Salary DESC


-- So, what is the difference between EXCEPT and NOT IN operators

-- 1.     Except filters duplicates and returns only DISTINCT rows from 
--        the left query that aren’t in the right query’s results,
--        where as NOT IN does not filter the duplicates.


-- 2.     EXCEPT operator expects the same number of columns in both 
--        the queries, where as NOT IN, compares a single column 
--        from the outer query with a single column from the subquery.










-- INTERSECT operator --- Get The Equals columns between two tables

SELECT Id, Name, Gender from TableB

INTERSECT

SELECT Id, Name, Gender from TableA






