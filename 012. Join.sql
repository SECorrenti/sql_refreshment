


Alter table tblEmployee
add DepatmentId int Not Null
constraint DeparmenIdDefault 
Default 1


Create table tblDepartment(
	ID int Primary key identity(1,1),
	DeparmentName nvarchar(120) NOT NULL,
	Location nvarchar(120) NOT NULL,
	DeparmentHead nvarchar(120) NOT NULL,
)

Select * from tblDepartment

--insert into tblDepartment values ('IT', 'London', 'Rick')
--insert into tblDepartment values ('Payroll', 'Delhi', 'Ron')
--insert into tblDepartment values ('HR', 'New York', 'Christie')
--insert into tblDepartment values ('Other Deparment', 'Sydney', 'Cindrella')


Alter table tblEmployee 
add Constraint FK_tblEmployee_DeparmentId___BELONGTO__tblDeparment_ID
Foreign key (DepatmentId) references tblDepartment(ID)


--INNER JOIN
--==========================================
Select Name, Gender, Salary, DeparmentName
from tblEmployee
Inner Join tblDepartment -- (or Join)
on tblEmployee.DepatmentId = tblDepartment.ID
--where Salary > 4000
--OR Gender = 'Female'



--(SELF) JOIN
--==========================================
--Select Name, Gender, Salary, DeparmentName
--from tblEmployee E
--Join tblEmployee M
--on tblEmployee.ManagerID = M.EmployeeID





--LEFT JOIN
--==========================================
Select Name, Gender, Salary, DeparmentName
from tblEmployee
Left Join tblDepartment -- (or Left Outer Join)
on tblEmployee.DepatmentId = tblDepartment.ID



--RIGHT JOIN
--==========================================
Select Name, Gender, Salary, DeparmentName
from tblEmployee
Right Join tblDepartment -- (or Right Outer Join)
on tblEmployee.DepatmentId = tblDepartment.ID




--FULL JOIN
--==========================================
Select Name, Gender, Salary, DeparmentName
from tblEmployee
Full Join tblDepartment -- (or Full Outer Join)
on tblEmployee.DepatmentId = tblDepartment.ID



--CROSS JOIN
--==========================================
Select Name, Gender, Salary, DeparmentName
from tblEmployee
CROSS Join tblDepartment





--CROSS APPLY - (part 91) inner join with functions
--==========================================
Select Name, Gender, Salary
from tblEmployee E
CROSS APPLY fnGetEmployeeByDepartmentId(E.DeptId) D




--OUTER APPLY - (part 91) outer join with functions
--==========================================
Select Name, Gender, Salary
from tblEmployee E
OUTER APPLY fnGetEmployeeByDepartmentId(E.DeptId) D






