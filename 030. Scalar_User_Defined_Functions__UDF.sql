



--Scalar functions may or may not have parameters, 
--but always return a single (scalar) value. 
--The returned value can be of any data type, 
--except text, ntext, image, cursor, and timestamp.





CREATE FUNCTION CalculateAge(@date DATE)
RETURNS INT
AS
BEGIN

	DECLARE @age INT
	SET @age = DATEDIFF(YEAR, @date, GETDATE()) -
		
			CASE
				WHEN (MONTH(@date) > MONTH(GETDATE())) OR 
					 (MONTH(@date) > MONTH(GETDATE())) AND DAY(@date) > Day(GETDATE())
				THEN 1
				ELSE 0
			END


	RETURN @Age

END


Select dbo.CalculateAge('01/20/1982')











