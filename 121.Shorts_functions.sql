


-- Using CASE statement in SQL Server

SELECT Name,
       DateOfBirth,
        CASE DATEPART(MM, DateOfBirth)
            WHEN 1 THEN 'JAN'
            WHEN 2 THEN 'FEB'
            WHEN 3 THEN 'MAR'
            WHEN 4 THEN 'APR'
            WHEN 5 THEN 'MAY'
            WHEN 6 THEN 'JUN'
            WHEN 7 THEN 'JUL'
            WHEN 8 THEN 'AUG'
            WHEN 9 THEN 'SEP'
            WHEN 10 THEN 'OCT'
            WHEN 11 THEN 'NOV'
            WHEN 12 THEN 'DEC'
        END
        AS [MONTH]
FROM Employees




-- Using CHOOSE function in SQL Server:
-- The amount of code we have to write is lot less than using CASE statement.


SELECT Name,
       DateOfBirth,
       CHOOSE(DATEPART(MM, DateOfBirth),
       'JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC')
       AS [MONTH]
FROM Employees




-- Using IIF function in SQL Server:

DECLARE @Gender INT
SET @Gender = 1
SELECT IIF(@Gender = 1, 'Male', 'Femlae') AS Gender



-- Using TRY_PARSE function in SQL Server:
-- Convert string to INT. The string cannot be converted to INT, so TRY_PARSE returns NULL
SELECT TRY_PARSE('99' AS INT) AS Result

-- Sample:
SELECT
    CASE  WHEN TRY_PARSE('ABC' AS INT) IS NULL
          THEN 'Conversion Failed'
          ELSE 'Conversion Successful'
    END 
AS Result


SELECT 
  IIF(TRY_PARSE('ABC' AS INT) IS NULL, 
      'Conversion Failed', 
      'Conversion Successful'
    )
AS Result








-- If you use PARSE instead of TRY_PARSE, the query fails with an error.
-- The query below returns the following error if the value are not a number
-- Error converting string value 'THIRTY' into data type int using culture

SELECT Name, PARSE(Age AS INT) AS Age
FROM Employees







-- TRY_CONVERT function

-- TRY_CONVERT function
--       Introduced in SQL Server 2012
--       Converts a value to the specified data type
--       Returns NULL if the provided value cannot be converted to the specified data type
--       If you request a conversion that is explicitly not permitted, then TRY_CONVERT fails with an error
--       Syntax : TRY_CONVERT ( data_type, value, [style] )


SELECT TRY_CONVERT(INT, '99') AS Result
SELECT TRY_CONVERT(INT, 'ABC') AS Result --- NULL
SELECT CONVERT(INT, '99') AS Result
SELECT CONVERT(INT, 'ABC') AS Result --- ERROR










-- EOMONTH function in SQL Server 2012


-- EOMONTH function (End of month)
--      Introduced in SQL Server 2012
--      Returns the last day of the month of the specified date

-- Syntax : EOMONTH ( start_date [, month_to_add ] )

-- Example : Returns last day of the month of February from a NON-LEAP year
SELECT EOMONTH('2/20/2015') AS LastDay

-- The following example adds 2 months to the start_date and returns the last day of the month from the resulting date
SELECT EOMONTH('3/20/2016', 2) AS LastDay


-- returns the last day of the month from the DateOfBirth of every employee.
SELECT Name, DateOfBirth, EOMONTH(DateOfBirth) AS LastDay
FROM Employees








-- DATEFROMPARTS function
--       Introduced in SQL Server 2012
--       Returns a date value for the specified year, month, and day
--       The data type of all the 3 parameters (year, month, and day) is integer
--       If invalid argument values are specified, the function returns an error
--       If any of the arguments are NULL, the function returns null
-- Syntax : DATEFROMPARTS ( year, month, day )


SELECT DATEFROMPARTS ( 2015, 10, 25) AS [Date]









-- DateTime2FromParts function
--       Introduced in SQL Server 2012
--       Returns DateTime2
--       The data type of all the parameters is integer
--       If invalid argument values are specified, the function returns an error
--       If any of the required arguments are NULL, the function returns null
--       If the precision argument is null, the function returns an error
-- Syntax : DATETIME2FROMPARTS ( year, month, day, hour, minute, seconds, fractions, precision )

-- Cannot construct data type datetime2, some of the arguments have values which are not valid.

SELECT DATETIME2FROMPARTS ( 2015, 11, 15, 20, 55, 55, 0, 0 ) AS [DateTime2]



































