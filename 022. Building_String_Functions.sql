




-- Return ascii number
Select ASCII('B')




-- Printing ABC



Declare @Start int
Set @Start = 65
While (@Start <= 90)
Begin
	Print CHAR(@Start) --		<------------- CHAR FN
	SET @Start = @Start + 1
END




Select LTRIM('     Hello')

Select RTRIM('Hello      ')

Select LOWER('Hello')

Select UPPER('Hello')

Select REVERSE('Hello')

Select LEN('Hello')



Select LEFT('Hello', 2)

Select RIGHT('Hello', 2)


Select  CHARINDEX('@', 'secorrenti@gmail.com')


Select SUBSTRING('secorrenti@gmail.com', 12, 5)

--- Repeat Some string
Select REPLICATE('Efi ', 5) ---- useful to build something like this se******@gmail.com

--- Repeate spaces ('     ')
Select SPACE(5)


Select Email, PATINDEX('%@gmail.com', Email) from tblPerson 
Where PATINDEX('%@gmail.com', Email) > 0



Select Email, REPLACE(Email, '.com', '.net') from tblPerson 


-- An other Masc Fn (REPLACE, FROM, TO, WITH WHAT)
Select Email, STUFF(Email, 2, 3, '*****') from tblPerson 

















