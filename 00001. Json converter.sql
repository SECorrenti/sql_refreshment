




CREATE
PROCEDURE spCartsUpdateExisting
	@ConnectionsIds VARCHAR(Max) OUTPUT
AS
BEGIN


	-- #### Parse JSON
	-- ###############################################

  SELECT * FROM vwCartsWithCartItemsCount Cart 
  WHERE Cart.Id NOT IN (
    SELECT CAST(Id AS INT) FROM OPENJSON(@CartsIds) WITH (Id INT)
  )
  ORDER BY Id DESC
  

	-- #### Parse JSON Object
	-- ###############################################
  -- #### To Make a strict data on Table use the strict value on string declaration  
  -- [DOB] INT 'strict $.user.details.dob'

  DECLARE @Json NVARCHAR(MAX);
  SET @Json = N'{
    "users": [
      { "user": { "id": 10, "name": "efi", "details": { "age": 38 } } },
      { "user": { "id": 12, "name": "sharon", "details": { "age": 36 } } }
    ]
  }';
  SELECT * FROM OPENJSON(@Json, '$.users') WITH (
    [Id] INT '$.user.id',
    [Name] NVARCHAR(50) '$.user.name',
    [Age] INT '$.user.details.age'
  );



  -- #### Get specific value in json
	-- ###############################################
  SELECT JSON_VALUE(@Json, '$.users[1].user.name')




  
  -- #### Check if is a valid JSON
	-- ###############################################
  SELECT ISJSON(@Json)



  
  -- #### Change the value based on the JSON path specified.
	-- ###############################################
  SELECT JSON_MODIFY(@Json, '$.users', '[]')




  
	-- #### Stringify JSON
	-- ###############################################

	SET @ConnectionsIds = (
		SELECT Id
		FROM Users 
		WHERE SecretId = @SecretId
		FOR JSON PATH
  -- #### To cover the result on some object, Use the root function   
  -- FOR JSON PATH, root('connectionsId')
	);




End

Go





  private async Task<HttpStatusCode> AddNewCart(SqlConnection con)
  {
      using (SqlCommand Command = new SqlCommand(CartsSqlListenets.CartsAddNew, con))
      {
          Command.CommandType = CommandType.StoredProcedure;
          Kies.AddWithValue(Command, Kies.SecretId, Model.Auth.SecretId);
          Kies.AddWithValue(Command, Kies.Name, Model.Name);
          Kies.AddWithValue(Command, Kies.Description, Model.Description);
          Kies.AddWithValue(Command, Kies.Protection, Model.Protection);
          var newId = Kies.Output(Command, Kies.Id, SqlDbType.Int);
          var accountId = Kies.Output(Command, Kies.AccountId, SqlDbType.Int);
          var connectionsIds = Kies.Output(Command, Kies.ConnectionsIds, SqlDbType.NVarChar, int.MaxValue);

          await con.OpenAsync();
          int rowAffected = await Command.ExecuteNonQueryAsync();
          if(rowAffected == 0)
          {
              return HttpStatusCode.Conflict;
          }
          Entity = Carts.CreateFromDto(Model, (int)accountId.Value, (int)newId.Value);
          Model.connectionIds = UsersConnections.GetConnections((string)connectionsIds.Value);
      }
      return HttpStatusCode.Created;
  }



  