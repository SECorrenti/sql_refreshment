select  * from tblEmployee

sp_helptext spGetEmployees


--CREATE PROC spGetEmployees (you can use PROC instead PROCEDURE)
CREATE PROCEDURE spGetEmployees
AS
BEGIN
	--- Put Your Logic Code Here
	--- Sample: 
	Select Name, Gender From tblEmployee
END





--Alter PROC spGetEmployees (Just replace Create to alter)
ALTER PROCEDURE spGetEmployees
AS
BEGIN
	--- Put Your Logic Code Here
	--- Sample: 
	Select Name, Gender From tblEmployee order by Name
END





--Drop PROC spGetEmployees
Drop PROCEDURE spGetEmployees





--	Encrypt PROC spGetEmployees (You cannot see procedure)
Create PROCEDURE spGetEmployees
With Encryption
AS
BEGIN
	--- Put Your Logic Code Here
	--- Sample: 
	Select Name, Gender From tblEmployee order by Name
END







-- Call Store Procedure
spGetEmployees
EXEC spGetEmployees
EXECUTE spGetEmployees







--============================================================
--============================================================
--============================================================
--============================================================





-- Create with input parameters
Create Proc spGetEmployeesByGenderAndDepartment
@Gender nvarchar(20),
@DepartmentId int
as
Begin

	Select Name, Gender, DepatmentId from tblEmployee 
	Where Gender = @Gender and DepatmentId = @DepartmentId

End


-- Call SP with parameters
spGetEmployeesByGenderAndDepartment 'Male', 1

-- OR

spGetEmployeesByGenderAndDepartment @DepartmentId = 1, @Gender = 'Male'





--============================================================
--============================================================
--============================================================
--============================================================





-- Create with OUTPUT parameters (You can use OUT)
Create Proc spGetEmployeesCountByGender
@Gender nvarchar(20),
@EmployeeCount int OUTPUT
as
Begin

	Select @EmployeeCount = COUNT(ID)
	from tblEmployee 
	Where Gender = @Gender

End

--- Call 
Declare @EmployeeTotal int
EXEC spGetEmployeesCountByGender 'Male', @EmployeeTotal OUT
Print @EmployeeTotal









--============================================================
--============================================================
--============================================================
--============================================================








-- Create with RETURN parameters
Create Proc spGetEmployeesCountByGender2
@Gender nvarchar(20)
as
Begin

	return (Select COUNT(ID) from tblEmployee Where Gender = @Gender)

End

--- Call 
Declare @EmployeeTotal int
EXEC @EmployeeTotal = spGetEmployeesCountByGender2 'Male'
Print @EmployeeTotal







--The following advantages of using Stored Procedures over adhoc queries (inline SQL)


--1. Execution plan retention and reusability - Stored Procedures are compiled and their
--		execution plan is cached and used again, when the same SP is executed again. Although adhoc
--		queries also create and reuse plan, the plan is reused only when the query is textual match
--		and the datatypes are matching with the previous call. Any change in the datatype or you have
--		an extra space in the query then, a new plan is created.


--2. Reduces network traffic - You only need to send, EXECUTE SP_Name statement, 
--		over the network, instead of the entire batch of adhoc SQL code.


--3. Code reusability and better maintainability - A stored procedure can be reused with 
--		multiple applications. If the logic has to change, we only have one place to change, 
--		where as if it is inline sql, and if you have to use it in multiple applications, 
--		we end up with multiple copies of this inline sql. If the logic has to change, we have
--		to change at all the places, which makes it harder maintaining inline sql.


--4. Better Security - A database user can be granted access to an SP and prevent 
--		them from executing direct "select" statements against a table.  This is fine grain
--		access control which will help control what data a user has access to.


--5. Avoids SQL Injection attack - SP's prevent sql injection attack.







--- Optional/Default Parameters

Create Proc spSomeOtherProc
@Gender nvarchar(20) = null,
as
Begin

	Select @EmployeeCount = COUNT(ID)
	from tblEmployee 
	Where Gender = @Gender OR @gender is null

End
 


