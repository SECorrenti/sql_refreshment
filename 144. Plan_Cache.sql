

-- Execute the following query to retrieve what we have in the query plan cache

SELECT cp.usecounts, cp.cacheobjtype, cp.objtype, st.text, qp.query_plan
FROM sys.dm_exec_cached_plans AS cp
CROSS APPLY sys.dm_exec_sql_text(plan_handle) AS st
CROSS APPLY sys.dm_exec_query_plan(plan_handle) AS qp
ORDER BY cp.usecounts DESC


-- If you use QUOTENAME() function, you can prevent sql injection while using Exec()
-- Cached query plan reusability is also not an issue while using Exec(), 
-- as SQL server automatically parameterize queries.
-- I personally prefer using sp_executesql over exec() 
-- as we can explicitly parameterise queries instead of
-- relying on sql server auto-parameterisation feature
-- or QUOTENAME() function. I use Exec() only in throw
-- away scripts rather than in production code.



