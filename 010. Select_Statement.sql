USE [Sample3]
GO

SELECT [ID]
      ,[Name]
      ,[Email]
      ,[GenderId]
      ,[Age]
  FROM  [dbo].[tblPerson]
GO


--- Is better to use * instead columns names (Performers issues)
SELECT * FROM tblPerson



Select DISTINCT Email from tblPerson



Select * from tblPerson where Email = 'a@a.com'



Select * from tblPerson where Email != 'a@a.com'
Select * from tblPerson where Email <> 'a@a.com'



Select * from tblPerson where Age = 20 or Age = 23 or Age = 29
Select * from tblPerson where Age IN (20, 23, 29)



Select * from tblPerson where Age BETWEEN 20 AND 29




Select * from tblPerson where Email LIKE 'L%'
Select * from tblPerson where Email LIKE '%@%'
Select * from tblPerson where Email NOT LIKE '%@%'

Select * from tblPerson where Email LIKE '_@_.com'
Select * from tblPerson where [Name] LIKE '[MST]%'
Select * from tblPerson where [Name] LIKE '[^MST]%' -- (not contains those characters)


insert into tblPerson values(4, 'Mary', 'b@b.com', 2, 50)


Select * from tblPerson where (Name = 'ABC' or Name = 'XYZ') And Age >= 20


Select * from tblPerson order by Name
Select * from tblPerson order by Name Asc
Select * from tblPerson order by Name Desc, Age



Select top 2 * from tblPerson
Select top 1 * from tblPerson Order by Age Desc
Select top 50 Percent * from tblPerson



Select SUM(Age) from tblPerson
Select MIN(Age) from tblPerson
Select MAX(Age) from tblPerson


--SQL Arithmetic Operators
--Operator	Description	Example

--	+   	Add	
--	-   	Subtract	
--	*   	Multiply	
--	/   	Divide	
--	%   	Modulo

--SQL Bitwise Operators
--Operator	Description

--	&   	Bitwise AND
--	|   	Bitwise OR
--	^   	Bitwise exclusive OR



--SQL Comparison Operators
--Operator	Description	Example

--	=   	Equal to	
--	>	    Greater than	
--	<   	Less than	
--	>=  	Greater than or equal to	
--	<=  	Less than or equal to	
--	<>  	Not equal to


--SQL Compound Operators
--Operator	Description

--	+=		Add equals
--	-=		Subtract equals
--	*=		Multiply equals
--	/=		Divide equals
--	%=		Modulo equals
--	&=		Bitwise AND equals
--	^-=		Bitwise exclusive equals
--	|*=		Bitwise OR equals

--SQL Logical Operators
--Operator	Description

--	ALL 	TRUE if all of the subquery values meet the condition	
--	AND 	TRUE if all the conditions separated by AND is TRUE	
--	ANY 	TRUE if any of the subquery values meet the condition	
--	BETWEEN	TRUE if the operand is within the range of comparisons	
--	EXISTS	TRUE if the subquery returns one or more records	
--	IN  	TRUE if the operand is equal to one of a list of expressions	
--	LIKE	TRUE if the operand matches a pattern	
--	NOT 	Displays a record if the condition(s) is NOT TRUE	
--	OR  	TRUE if any of the conditions separated by OR is TRUE	
--	SOME	TRUE if any of the subquery values meet the condition	








