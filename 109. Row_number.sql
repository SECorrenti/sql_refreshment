

-- In this example, data is not partitioned, so ROW_NUMBER 
-- will provide a consecutive numbering for all the rows in 
-- the table based on the order of rows imposed by the ORDER BY clause.


SELECT Name, Gender, Salary,
        ROW_NUMBER() OVER (ORDER BY Gender) AS RowNumber
FROM Employees





--  In this example, data is partitioned by Gender, so ROW_NUMBER 
--  will provide a consecutive numbering only for the rows with in 
--  a parttion. When the partition changes the row number is reset to 1.

SELECT Name, Gender, Salary,
        ROW_NUMBER() OVER (PARTITION BY Gender ORDER BY Gender) AS RowNumber
FROM Employees


































