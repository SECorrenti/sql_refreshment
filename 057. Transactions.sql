










Create Procedure spUpdateAddress
as
Begin

	 Begin Try

		  Begin Transaction

			   Update tblMailingAddress set City = 'LONDON' 
			   where AddressId = 1 and EmployeeNumber = 101
   
			   Update tblPhysicalAddress set City = 'LONDON' 
			   where AddressId = 1 and EmployeeNumber = 101

		  Commit Transaction

	 End Try
	 Begin Catch

		  Rollback Transaction

	 End Catch

End



-- You can write  TRAN instead TRANSACTION 





-- A transaction is a group of database commands that are treated as a single unit. 
-- A successful transaction must pass the "ACID" test, that is, it must be
--			A - Atomic
--			C - Consistent
--			I - Isolated
--			D - Durable







-- Another Sample: 
-- Transfer $100 from Mark to Mary Account

BEGIN TRY
    BEGIN TRANSACTION
         UPDATE Accounts SET Balance = Balance - 100 WHERE Id = 1
         UPDATE Accounts SET Balance = Balance + 100 WHERE Id = 2
    COMMIT TRANSACTION
    PRINT 'Transaction Committed'
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION
    PRINT 'Transaction Rolled back'
END CATCH



-- Billing the customer
Waitfor Delay '00:00:15'
-- Insufficient Funds. Rollback transaction




-- make the data to be unreal ---- should be dirty
Set Transaction Isolation Level Read Uncommitted -- <----- DO NOT USE THIS
Select * from tblInventory where Id=1
															---- another way with NOLOCK
Select * from tblInventory (NOLOCK) where Id=1



-- 			Read Uncommitted transaction isolation level is the only 
--			isolation level that has dirty read side effect. 
--			This is the least restrictive of all the isolation levels. 
--			When this transaction isolation level is set, it is possible 
--			to read uncommitted or dirty data. Another option to read 
--			dirty data is by using NOLOCK table hint. The query below is
--			equivalent to the query in Transaction 2.

-- Back it with
Set Transaction Isolation Level Read committed



--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------




-- Lock and Fail other transactions
Set Transaction Isolation level Repeatable READ




--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------




-- Lock other transactions
Set Transaction Isolation level serializable




--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------
--------------------------------------------



-- To Use SNAPSHOT Mode on DB -- enable it
ALTER DATABASE SAMPLEDB
SET ALLOW_SNAPSHOT_ISOLATION ON

Set Transaction Isolation level snapshot




-- What is the difference between serializable and snapshot isolation levels
-- Serializable isolation is implemented by acquiring locks which means
-- the resources are locked for the duration of the current transaction. 
-- This isolation level does not have any concurrency side effects but at 
-- the cost of significant reduction in concurrency.

-- Snapshot isolation doesn't acquire locks, it maintains versioning in Tempdb.
-- Since, snapshot isolation does not lock resources, it can significantly increase
-- the number of concurrent transactions while providing the same level
-- of data consistency as serializable isolation does.




Alter database SampleDB
SET READ_COMMITTED_SNAPSHOT ON







SET DEADLOCK_PRIORITY HIGH
SET DEADLOCK_PRIORITY MEDIUM
SET DEADLOCK_PRIORITY LOW


-- logging deadlocks

DBCC Traceon(1222, -1)
DBCC TraceStatus(1222)
DBCC Traceoff(1222)

execute sp_readerrorlog


IF (ERROR_NUMBER() = 1205) 
BEGIN	
	Select 'Deadlock. Transaction failed. Please retry'
END



-- try
-- {
-- 		string cs = ConfigurationManager.ConnectionStrings["DBCS"].ConnectionString;
-- 		using (SqlConnection con = new SqlConnection(cs))
-- 		{
-- 				SqlCommand cmd = new SqlCommand("spTransaction1", con);
-- 				cmd.CommandType = CommandType.StoredProcedure;
-- 				con.Open();
-- 				cmd.ExecuteNonQuery();
-- 				Label1.Text = "Transaction successful";
-- 				Label1.ForeColor = System.Drawing.Color.Green;
-- 		}
-- }
-- catch (SqlException ex)
-- {
-- 		if (ex.Number == 1205)
-- 		{
-- 				Label1.Text = "Deadlock. Please retry";
-- 		}
-- 		else
-- 		{
-- 				Label1.Text = ex.Message;
-- 		}
-- 		Label1.ForeColor = System.Drawing.Color.Red;
-- }




-- How to find blocking queries in sql server
DBCC OpenTran --<------Untrusted 





-- 		Here’s a little script I knocked up this afternoon to 
-- 		tell me who has open transactions on the server 
-- 		– not just the single oldest active transaction that DBCC OPENTRAN returns.


-- It gives back:

-- 				session ID
-- 				login name
-- 				database context
-- 				transaction begin time
-- 				how many log records have been generated by the transaction
-- 				how much log space has been taken up by those log records
-- 				how much log space has been reserved in case the transaction rolls back
-- 				the last T-SQL that was executed in the context of the transaction
-- 				the last query plan that was executed (only for currently executing plans)

SELECT
    [s_tst].[session_id],
    [s_es].[login_name] AS [Login Name],
    DB_NAME (s_tdt.database_id) AS [Database],
    [s_tdt].[database_transaction_begin_time] AS [Begin Time],
    [s_tdt].[database_transaction_log_bytes_used] AS [Log Bytes],
    [s_tdt].[database_transaction_log_bytes_reserved] AS [Log Rsvd],
    [s_est].text AS [Last T-SQL Text],
    [s_eqp].[query_plan] AS [Last Plan]
FROM
    sys.dm_tran_database_transactions [s_tdt]
JOIN
    sys.dm_tran_session_transactions [s_tst]
ON
    [s_tst].[transaction_id] = [s_tdt].[transaction_id]
JOIN
    sys.[dm_exec_sessions] [s_es]
ON
    [s_es].[session_id] = [s_tst].[session_id]
JOIN
    sys.dm_exec_connections [s_ec]
ON
    [s_ec].[session_id] = [s_tst].[session_id]
LEFT OUTER JOIN
    sys.dm_exec_requests [s_er]
ON
    [s_er].[session_id] = [s_tst].[session_id]
CROSS APPLY
    sys.dm_exec_sql_text ([s_ec].[most_recent_sql_handle]) AS [s_est]
OUTER APPLY
    sys.dm_exec_query_plan ([s_er].[plan_handle]) AS [s_eqp]
ORDER BY
    [Begin Time] ASC;
GO



 


--    protected void btnTransfer_Click(object sender, EventArgs e)
--     {
--         string cs = ConfigurationManager.ConnectionStrings["CS"].ConnectionString;

--         using (SqlConnection con = new SqlConnection(cs))
--         {
--             con.Open();
--             // Begin a transaction. The connection needs to 
--             // be open before we begin a transaction
--             SqlTransaction transaction = con.BeginTransaction();
--             try
--             {
--                 // Associate the first update command with the transaction
--                 SqlCommand cmd = new SqlCommand
--                     ("Update Accounts set Balance = Balance - 10 where AccountNumber = 'A1'"
--                     , con, transaction);
--                 cmd.ExecuteNonQuery();
--                 // Associate the second update command with the transaction
--                 cmd = new SqlCommand("Update Accounts set Balance = Balance + 10 where AccountNumber = 'A2'"
--                     , con, transaction);
--                 cmd.ExecuteNonQuery();
--                 // If all goes well commit the transaction
--                 transaction.Commit();
--                 lblMessage.ForeColor = System.Drawing.Color.Green;
--                 lblMessage.Text = "Transaction committed";
--             }
--             catch
--             {
--                 // If anything goes wrong, rollback the transaction
--                 transaction.Rollback();
--                 lblMessage.ForeColor = System.Drawing.Color.Red;
--                 lblMessage.Text = "Transaction rolled back";
--             }
--         }
--         GetAccountsData();
--     }
