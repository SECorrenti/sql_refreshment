
Use [Sample3]
Go

Create table tblPerson (
	ID int NOT NULL Primary Key,
	Name nvarchar(150) NOT NULL,
	Email nvarchar(150) NOT NULL,
	GenderId int NOT NULL,
)
Go

Create table tblGender (
	ID int NOT NULL Primary Key,
	Gender nvarchar(50) NOT NULL
)
Go


Alter Table tblPerson add constraint FK_tblPerson_genderId_BELONGTO_tblGender_id
Foreign key (genderId) references tblGender(Id)
Go


insert into [tblPerson](ID, Name, Email, GenderId) values (3, 'Simon', 's@s.com', 1)
insert into [tblPerson](ID, Name, Email, GenderId) values (4, 'Sam', 'sam@sam.com', 1)
insert into [tblPerson](ID, Name, Email, GenderId) values (5, 'May', 'may@may.com', 2)
insert into [tblPerson](ID, Name, Email, GenderId) values (6, 'Kenny', 'k@k.com', 3)
insert into [tblPerson](ID, Name, Email) values (7, 'efi', 'efi@efi.com')


alter table [tblPerson] 
add constraint DF_tblPerson_genderId
Default 3 for GenderId


alter table [tblPerson]
add Age int not null
constraint DF_tblPerson_age default 20

--Alter table [tblPerson]
--Drop constraint tblPerson_genderId_BELONGTO_tblGender_id
--Go


alter table [tblPerson]
add Constraint CK_tblPerson_Age_should_be_a_valid_age
Check (Age > 0 and Age < 150) 

--insert into [tblPerson](ID, Name, Email, GenderId, Age) values (8, 'Simon', 's@s.com', 1, -500)


CREATE TABLE tblOrders (
    OrderID int NOT NULL PRIMARY KEY,
    OrderNumber int NOT NULL,
    PersonID int FOREIGN KEY REFERENCES tblPerson(ID)
);

CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    CONSTRAINT FK_PersonOrder FOREIGN KEY (PersonID)
    REFERENCES TblPerson(ID)
);

Drop table tblOrders;
Drop table Orders;



Create Table tblAccounts(
	Id int primary key not null,
	Name nvarchar(150) NOT NULL
)

-- Set identity insert ON if you wan to add ids manually (And OFF if you wan to block it back)
SET IDENTITY_INSERT tblAccounts ON


delete from tblPerson

-- Reset table to 0 identity
DBCC CHECKIDENT(tblPerson, RESEED, 0)


