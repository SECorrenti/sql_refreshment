







-- ABS = ABSOLUTE number (positive)
Select ABS(-105.5) --- return 105.5 whitout the - sign



Select CEILING(15.2) -- return 16
Select CEILING(-15.2) -- return -15

Select FLOOR(15.2) -- return 15
Select FLOOR(-15.2) -- return -16



Select POWER(2, 3) -- return 8


Select SQUARE(9) -- return 81
Select SQRT(81) -- return 9



Select RAND()
Select FLOOR(RAND() * 100)


Select ROUND(850.5869864, 2) -- return 850.5900000

-- 1 meens if you wan to truncate the number
Select ROUND(850.5869864, 2, 1) -- return 850.5800000

Select ROUND(850.5849864, -2)
