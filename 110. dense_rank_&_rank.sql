

-- Rank and Dense_Rank functions in SQL Server



-- Rank and Dense_Rank functions
--       Introduced in SQL Server 2005
--       Returns a rank starting at 1 based on the ordering of rows imposed by the ORDER BY clause
--       ORDER BY clause is required
--       PARTITION BY clause is optional
--       When the data is partitioned, rank is reset to 1 when the partition changes
--       Difference between Rank and Dense_Rank functions
--       Rank function skips ranking(s) if there is a tie where as Dense_Rank will not.

-- For example : If you have 2 rows at rank 1 and you have 5 rows in total.
-- RANK() returns - 1, 1, 3, 4, 5
-- DENSE_RANK returns - 1, 1, 2, 3, 4

-- Syntax : 
-- RANK() OVER (ORDER BY Col1, Col2, ...)
-- DENSE_RANK() OVER (ORDER BY Col1, Col2, ...)



-- SQL Script to create Employees table
Create Table Employees
(
    Id int primary key,
    Name nvarchar(50),
    Gender nvarchar(10),
    Salary int
)
Go

Insert Into Employees Values (1, 'Mark', 'Male', 8000)
Insert Into Employees Values (2, 'John', 'Male', 8000)
Insert Into Employees Values (3, 'Pam', 'Female', 5000)
Insert Into Employees Values (4, 'Sara', 'Female', 4000)
Insert Into Employees Values (5, 'Todd', 'Male', 3500)
Insert Into Employees Values (6, 'Mary', 'Female', 6000)
Insert Into Employees Values (7, 'Ben', 'Male', 6500)
Insert Into Employees Values (8, 'Jodi', 'Female', 4500)
Insert Into Employees Values (9, 'Tom', 'Male', 7000)
Insert Into Employees Values (10, 'Ron', 'Male', 6800)
Go



-- RANK() and DENSE_RANK() functions without PARTITION BY clause: 
-- In this example, data is not partitioned, so RANK() function 
-- provides a consecutive numbering except when there is a tie. 
-- Rank 2 is skipped as there are 2 rows at rank 1. The third row gets rank 3.


-- DENSE_RANK() on the other hand will not skip ranks if there is a tie.
-- The first 2 rows get rank 1. Third row gets rank 2.


SELECT Name, Salary, Gender,
RANK() OVER (ORDER BY Salary DESC) AS [Rank],
DENSE_RANK() OVER (ORDER BY Salary DESC) AS DenseRank
FROM Employees







-- RANK() and DENSE_RANK() functions with PARTITION BY clause:
-- Notice when the partition changes from Female to Male Rank is reset to 1

SELECT Name, Salary, Gender,
RANK() OVER (PARTITION BY Gender ORDER BY Salary DESC) AS [Rank],
DENSE_RANK() OVER (PARTITION BY Gender ORDER BY Salary DESC)
AS DenseRank
FROM Employees










-- Insert new rows with duplicate valuse for Salary column
Insert Into Employees Values (1, 'Mark', 'Male', 8000)
Insert Into Employees Values (2, 'John', 'Male', 8000)
Insert Into Employees Values (3, 'Pam', 'Female', 8000)
Insert Into Employees Values (4, 'Sara', 'Female', 4000)
Insert Into Employees Values (5, 'Todd', 'Male', 3500)







-- Notice 3 employees have the same salary 8000.
-- When you execute the following query you can clearly see
-- the difference between RANK, DENSE_RANK and ROW_NUMBER functions.

SELECT Name, Salary, Gender,
ROW_NUMBER() OVER (ORDER BY Salary DESC) AS RowNumber,
RANK() OVER (ORDER BY Salary DESC) AS [Rank],
DENSE_RANK() OVER (ORDER BY Salary DESC) AS DenseRank
FROM Employees









-- Difference between RANK, DENSE_RANK and ROW_NUMBER functions

-- ROW_NUMBER:  Returns an increasing unique number for each 
--               row starting at 1, even if there are duplicates.

-- RANK:        Returns an increasing unique number for each row 
--               starting at 1. When there are duplicates, same rank 
--               is assigned to all the duplicate rows, but the next row after 
--               the duplicate rows will have the rank it would have been 
--               assigned if there had been no duplicates. 
--               So RANK function skips rankings if there are duplicates.

-- DENSE_RANK:  Returns an increasing unique number for each row starting at 1.
--               When there are duplicates, same rank is assigned to
--               all the duplicate rows but the DENSE_RANK function will not skip any ranks. 
--               This means the next row after the duplicate
--               rows will have the next rank in the sequence.














-- JUST AN EXAMPLE: Calculate running total


-- SQL Script to create Employees table
Create Table Employees
(
     Id int primary key,
     Name nvarchar(50),
     Gender nvarchar(10),
     Salary int
)
Go

Insert Into Employees Values (1, 'Mark', 'Male', 5000)
Insert Into Employees Values (2, 'John', 'Male', 4500)
Insert Into Employees Values (3, 'Pam', 'Female', 5500)
Insert Into Employees Values (4, 'Sara', 'Female', 4000)
Insert Into Employees Values (5, 'Todd', 'Male', 3500)
Insert Into Employees Values (6, 'Mary', 'Female', 5000)
Insert Into Employees Values (7, 'Ben', 'Male', 6500)
Insert Into Employees Values (8, 'Jodi', 'Female', 7000)
Insert Into Employees Values (9, 'Tom', 'Male', 5500)
Insert Into Employees Values (10, 'Ron', 'Male', 5000)
Go

-- SQL Query to compute running total without partitions
SELECT Name, Gender, Salary,
        SUM(Salary) OVER (ORDER BY ID) AS RunningTotal
FROM Employees



-- SQL Query to compute running total with partitions
SELECT Name, Gender, Salary,
        SUM(Salary) OVER (PARTITION BY Gender ORDER BY ID) AS RunningTotal
FROM Employees






















